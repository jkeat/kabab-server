const express = require('express');
const fetch = require('node-fetch');
const _ = require('lodash');
const cheerio = require('cheerio');
const cheerioTableparser = require('cheerio-tableparser');
const colorGen = require('color-generator');
const numeral = require('numeral');
const PORT = process.env.PORT || 5000;

function generateColor() {
  const color = colorGen(0.5, 0.9);

  const hsl = color.hsl();

  if (hsl.h > 35 && hsl.h < 90) {
    return generateColor();
  }

  return color.hexString().substring(1);
}

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/parse', async (req, res) => {
  const parseUrl = req.query.url;
  const resp = await fetch(parseUrl);
  const htmlData = await resp.text();
  const $ = cheerio.load(htmlData);
  cheerioTableparser($);

  // this check allows wiki or regular sites.
  // TODO: cleaner, flexible for more 'site funnels' structured in sep. files/classes.
  const $tables = $('table.wikitable');

  $tables.find('.navbar, .reference, .vcard').remove();

  const parsedTables = _.map(_.range($tables.length), (idx) => {    
    const $table = $tables.eq(idx);
    const $tableClone = $table.clone();

    $table.find('tr:has(th)').remove();
    const tableDataCols = $table.parsetable(true, true, true);


    $tableClone.find('tr:has(td)').remove();
    let tableHeaderCols = $tableClone.parsetable(true, true, true);

    // wikipedia's tables are super inconsistent
    // if there was no table header, it's probably the first data row
    if (tableHeaderCols.length === 0) {
      tableHeaderCols = _.map(tableDataCols, (col) => {
        return [col.shift()]; // remove first data table col item
      });
    }

    const headerCols = _.map(tableHeaderCols, (headerCol, idx) => {
      const lastCell = _.last(headerCol);
      return {
        name: lastCell,
        index: idx,
        type: 'string', // TODO! set
      };
    });

    const typedTableDataCols = _.map(tableDataCols, (col) => {
      const stringCol = _.map(col, (cellVal) => {
        const cell = cellVal === undefined ? '' : cellVal;
        const cleanCell = cell.replace(/†/g, '');
        return cleanCell;
      });

      let numberCount = 0;
      const colCount = stringCol.length;

      const numberCol = _.map(stringCol, (cell) => {
        const numberRegex = /([\d,.]+)/;
        const regexMatch = cell.match(numberRegex);
        const firstNumber = regexMatch ? regexMatch[0] : null;
        let parsedNumber = numeral(firstNumber).value();

        if (typeof parsedNumber === 'number') { numberCount++ }
        else { /* parsedNumber = 0; */ } // pick a default and coordinate w/ frontend

        return parsedNumber;
      });

      const percentNumbers = numberCount / colCount;
      const isNumberCol = percentNumbers > 0.5; // TODO: percent of non-null cells, not just all cells

      return isNumberCol ? numberCol : stringCol;
    });

    const tableDataRows = _.zip(...typedTableDataCols);

    // Guess table name
    // TODO: number the tables if multiple share a title
    // TODO: if no .mw-headline, use article title, plus number if multiple
    const tableTitle = $table.prevAll('h1,h2,h3').first().find('.mw-headline').text() || `Table ${idx + 1}`;

    return {
      name: tableTitle,
      colHeaders: headerCols,
      rows: tableDataRows,
      color: generateColor(),
      id: _.uniqueId('table_'), // TODO: get ID from database!!
    };
  });

  const tables = _.filter(parsedTables, (table) => {
    if (table.rows.length < 7) { return false; }
    if (table.colHeaders.length < 3) { return false; }

    return true;
  });

  res.send({
    tables: tables,
    url: parseUrl,
  });
});

app.listen(PORT, () => console.log(`App listening on port ${PORT}!`));
